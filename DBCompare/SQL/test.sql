alter procedure test
as
set nocount on
DECLARE @account_id int, @j int, @i int;
delete from admin_all.account_tran;
delete from admin_all.account;
delete from external_mpt.user_conf;
DBCC CHECKIDENT('external_mpt.user_conf', reseed, 0);
DBCC CHECKIDENT('admin_all.account', reseed, 0);
    
select @i=0;
while @i<100
begin
	begin transaction
	insert into external_mpt.USER_CONF(USERID, ACTIVE_FLAG, BRANDID, LOCKED_STATUS, currency, 
									user_type,  FIRST_NAME, LAST_NAME, bonus, language, 
									logonmsg_flag, reg_date, maillist_flag, registration_status, login_attempts,
									reality_check_interval, session_limit_interval, made_deposit, allow_emails, winners_list,
									kyc_status, withdrawal_threshold_verified, kyc_age_status, iovation_status, 
									is_autopay, iovation_check, is_transferable, display_message)
		values( 
									admin_all.random_between(1,100000000), '1', admin_all.random_between(1,100000000),  'NOT_LOCKED','USD', 
									1,  admin_all.random_between(1,10000), admin_all.random_between(1,10000) , admin_all.random_between(1,10000), 'en', 
									'1', getdate(), '1', 'reg', admin_all.random_between(1,1000), 
									admin_all.random_between(1,1000), admin_all.random_between(1,1000), 1, 1, 1, 
									'a', 1, '123', '123', 
									1, 1, 1, 1);
			
	insert into admin_all.account(partyid, balance_real, released_bonus, playable_bonus, raw_loyalty_points, secondary_balance, unpaid_cit, cit)
		values(@@identity, 0, 0,0,0,0,0,0);
	commit ;
	select @j=0;
	while(@j<1000)
	begin
		begin transaction;
		select @account_id = admin_all.random_between(1,(select max(ID) from admin_all.account));
		update admin_all.account set balance_real = balance_real where id = @account_id;
		insert into admin_all.account_tran(
						account_id, datetime,tran_type,amount_real,balance_real,
						platform_tran_id, game_tran_id, game_id, platform_id, payment_id,
						rolled_back, rollback_tran_id, amount_released_bonus, amount_playable_bonus, balance_released_bonus,
						balance_playable_bonus, amount_underflow, amount_raw_loyalty, balance_raw_loyalty, transaction_on_hold_id,
						ssw_tran_id, reference, brand_id)
			values(@account_id, getdate(),'tran_type',0,0,
						'dfafadfaad', 'dfadfadfafa', 'game_id', 1, null,
						0, null, 0, 0, 0,
						0, 0, 0, 0, null,
						null, 'dfadfadfa', 3);
		update admin_all.account set balance_real = admin_all.random_between(1,100000000) where id = @account_id;
		select @j=@j+1;
		commit;
	end;
	select @i=@i+1;
end;

--exec test;