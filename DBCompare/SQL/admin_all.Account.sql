
CREATE TABLE admin_all.ACCOUNT(
	id int NOT null identity(1,1),
	PARTYID int NOT NULL,
	BALANCE_REAL numeric(38, 18) NOT NULL,
	RELEASED_BONUS numeric(38, 18) NOT NULL,
	PLAYABLE_BONUS numeric(38, 18) NOT NULL,
	RAW_LOYALTY_POINTS bigint NOT NULL,
	SECONDARY_BALANCE numeric(38, 18) NOT NULL,
	ROWVERSION timestamp NOT NULL,
	UNPAID_CIT numeric(38, 18) NOT NULL,
	CIT numeric(38, 18) NOT null
	);

alter table admin_all.ACCOUNT add constraint pk_account primary key (id);


CREATE UNIQUE  INDEX IDX_admin_all_ACCOUNT_PARTYID ON admin_all.ACCOUNT
(
	PARTYID ASC
);


