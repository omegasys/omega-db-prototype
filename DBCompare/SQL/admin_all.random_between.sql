create view admin_all.Random
as
select rand() as R

go

alter FUNCTION admin_all.random_between(@low int ,@high int) 
RETURNS INT as
BEGIN
   RETURN (select floor(r* (@high-@low ) + @low) from admin_all.Random)
END;
GO

--select admin_all.random_between(1,100)

