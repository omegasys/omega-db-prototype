/*ALTER SEQUENCE external_mpt.user_conf_partyid_seq RESTART WITH 1;
ALTER SEQUENCE admin_all.account_id_seq RESTART WITH 1;
delete from admin_all.account_tran;
delete from admin_all.account;
delete from external_mpt.user_conf;
*/
DO $$ 

DECLARE
	account_id int;
	j int;
 	i int;
begin
	ALTER SEQUENCE external_mpt.user_conf_partyid_seq RESTART WITH 1;
	ALTER SEQUENCE admin_all.account_id_seq RESTART WITH 1;
	delete from admin_all.account_tran;
	delete from admin_all.account;
	delete from external_mpt.user_conf;
	i=0;
	loop
		exit when i = 100;
		insert into external_mpt.USER_CONF(USERID, ACTIVE_FLAG, BRANDID, LOCKED_STATUS, currency, 
										user_type,  FIRST_NAME, LAST_NAME, bonus, language, 
										logonmsg_flag, reg_date, maillist_flag, registration_status, login_attempts,
										reality_check_interval, session_limit_interval, made_deposit, allow_emails, winners_list,
										kyc_status, withdrawal_threshold_verified, kyc_age_status, iovation_status, 
										is_autopay, iovation_check, is_transferable, display_message)
			values( 
										cast(admin_all.random_between(1,100000000) as text), '1', admin_all.random_between(1,100000000),  'NOT_LOCKED','USD', 
										1,  cast(admin_all.random_between(1,10000) as text), cast(admin_all.random_between(1,10000) as text), admin_all.random_between(1,10000), 'en', 
										'1', current_date, '1', 'reg', admin_all.random_between(1,1000), 
										admin_all.random_between(1,1000), admin_all.random_between(1,1000), 1, B'1', B'1', 
										'a', B'1', '123', '123', 
										B'1', B'1', B'1', B'1');
			
		insert into admin_all.account(partyid, balance_real, released_bonus, playable_bonus, raw_loyalty_points, secondary_balance, unpaid_cit, cit)
			values(currval('external_mpt.user_conf_partyid_seq'), 0, 0,0,0,0,0,0);
		commit ;
		j=0;
		loop
			exit when j=1000;
			account_id = admin_all.random_between(1,(select max(ID) from admin_all.account));
			update admin_all.account set balance_real = balance_real where id = account_id;
			insert into admin_all.account_tran(
							account_id, datetime,tran_type,amount_real,balance_real,
							platform_tran_id, game_tran_id, game_id, platform_id, payment_id,
							rolled_back, rollback_tran_id, amount_released_bonus, amount_playable_bonus, balance_released_bonus,
							balance_playable_bonus, amount_underflow, amount_raw_loyalty, balance_raw_loyalty, transaction_on_hold_id,
							ssw_tran_id, reference, brand_id)
				values(account_id, current_date,'tran_type',0,0,
							'dfafadfaad', 'dfadfadfafa', 'game_id', 1, null,
							0, null, 0, 0, 0,
							0, 0, 0, 0, null,
							null, 'dfadfadfa', 3);
			update admin_all.account set balance_real = admin_all.random_between(1,100000000) where id = account_id;
			j=j+1;
			commit;
		end loop;
		i=i+1;
	end loop;
end;
    $$;
