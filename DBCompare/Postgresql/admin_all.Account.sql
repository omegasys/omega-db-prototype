CREATE TABLE admin_all.ACCOUNT(
	id int NOT null generated always as identity,
	PARTYID int NOT NULL,
	BALANCE_REAL numeric(38, 18) NOT NULL,
	RELEASED_BONUS numeric(38, 18) NOT NULL,
	PLAYABLE_BONUS numeric(38, 18) NOT NULL,
	RAW_LOYALTY_POINTS bigint NOT NULL,
	SECONDARY_BALANCE numeric(38, 18) NOT NULL,
	ROWVERSION timestamp NOT NULL,
	UNPAID_CIT numeric(38, 18) NOT NULL,
	CIT numeric(38, 18) NOT null
	);

alter table admin_all.ACCOUNT add primary key (id);
cluster admin_all.ACCOUNT using  account_pkey;

CREATE UNIQUE  INDEX IDX_admin_all_ACCOUNT_PARTYID ON admin_all.ACCOUNT
(
	PARTYID ASC
);


