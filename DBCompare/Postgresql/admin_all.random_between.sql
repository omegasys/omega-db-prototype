CREATE OR REPLACE FUNCTION admin_all.random_between(low INT ,high INT) 
   RETURNS INT as
$$
BEGIN
   RETURN floor(random()* (high-low) + low);
END;
$$ language 'plpgsql' STRICT;