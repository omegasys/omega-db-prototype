# MariaDB v.s. PostgreSQL

| Features      | MariaDB                          | PostgreSQL              | Comments                                                                                                                                                                                                                                                          |
| :------------ | :------------------------------- | :---------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| NOSQL Support | Some                             | Yes                     |                                                                                                                                                                                                                                                                   |
| Architecture  | Multi Thread                     | Multi Process           | As Postgres forks off a child process to establish a connection, it can take up to 10 MB per connection. The memory pressure is bigger compared to MySQL's thread-per-connection model, where the default stack size of a thread is at 256KB on 64-bit platforms. |
| Cluster       | One-way asynchronous replication | Synchronous replication | With synchronous replication, each write waits until confirmation is received from both master and slave.                                                                                                                                                         |

## Cluster

| MariaDB                                                                                                                                                                                                                                                                                                                                                                                                                  | PostgreSQL                                                                                                                                                                                                                                                                                                                                                                  |
| :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| MySQL Cluster is a technology providing shared-nothing (no single point of failure) clustering and auto-sharding (partitioning) for the MySQL database management system. Internally MySQL Cluster uses synchronous replication through a two-phase commit mechanism to guarantee that data is written to multiple nodes. This contrasts with what is usually referred to as "MySQL Replication", which is asynchronous. | PostgreSQL has synchronous replication (called 2-safe replication), that utilizes two database instances running simultaneously where your master database is synchronized with a slave database. Unless both databases crash simultaneously, data won't be lost. With synchronous replication, each write waits until confirmation is received from both master and slave. |

---

## History

:memo: Lack of configuration flexibility with replication on PostgreSQL was the reason why Uber switched to MySQL.Not any more with logical replication.

---

## Load Tests

### Buck Insert Test

100,000 Records Insert, 2K in size within one transaction
| MSSQL 2017 | MariaDB 10.3 | PostgreSQL 11 | Comments |
|:--- |:--- |:--- |---|
| 1.9s | 4.5s | 12s | To support high transaction volume, Data has to be distributed to multiple servers. |

### Account Update Simulation Test

The performance test was done with reasonable complexity.

- User, Account, AccountTran tables with 25 indexes on all tables
- Perform User Creation, Account Update and AccountTran Insert to simulate simplest account lock & update as well as transaction insert
- See Pseudo Code below

```
Loop 100 times
	begin transaction
	insert User table
	Insert Account table
	Commit
	Loop 1000 times
		begin transaction
		Choose an account randomly
		Lock Account table
		Insert Account Tran
		Update Account balnace
	End Loop
End Loop
```

Total Duration In Seconds
| MS SQL(DelayedDurability) | MSSQL 2017 | MariaDB 10.3 | PostgreSQL 11 | Comments |
|:--- |:--- |:--- |:--- |---|
| 18.3s | 281.8s | 311.2s | 304.8s | Result are about the same for all databases tested, however, with advanced deplayed durability, SQL Server can have 10x better write performance, what is the equivlent in MariaDB or PostgreSQL is yet to be determined |

## Conclusion

---

## References

- [Showdown: MySQL 8 vs PostgreSQL 10](http://blog.dumper.io/showdown-mysql-8-vs-postgresql-10)
- [PostgreSQL vs. MySQL](https://blog.panoply.io/postgresql-vs.-mysql)
